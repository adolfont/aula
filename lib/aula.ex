defmodule Aula do
  def pi do
    3.1415
  end

  def identidade(x) do
    x
  end

  def oposto(x) do
    -x
  end

  def maior(x,y) do
    if x > y do
      x
    else
      y
    end
  end

  def maior_de_3(x,y,z) do
    maior(x,maior(y,z))
  end

  def maior_de_3_eh(x1,x2,x3,o_maior) do
    o_maior == maior(x1,maior(x2,x3))
  end
end

# Função que não recebe argumentos, aridade zero, zeroária
IO.puts "Pi=#{Aula.pi}"

# Função que recebe um argumento
IO.puts "3=#{Aula.identidade(3)}"
IO.puts "O oposto de 3 é #{Aula.oposto(3)}"

# Função que recebe dois argumentos
IO.puts "O maior número entre 3 e 4 é #{Aula.maior(3,4)}"
IO.puts "O maior número entre 5 e 4 é #{Aula.maior(5,4)}"

# Função que recebe dois argumentos
# E que utiliza outra função em sua definição
IO.puts "O maior número entre 5, 3 e 4 é #{Aula.maior_de_3(5,3,4)}"

# Função que retorna valor-verdade
IO.puts "O maior número entre 5, 3 e 4 é 5? #{Aula.maior_de_3_eh(5,3,4,5)}"
IO.puts "O maior número entre 38, 22 e 41 é 22? #{Aula.maior_de_3_eh(38,22,41,22)}"

# Hora de entrar com listas e testes automatizados!!!
